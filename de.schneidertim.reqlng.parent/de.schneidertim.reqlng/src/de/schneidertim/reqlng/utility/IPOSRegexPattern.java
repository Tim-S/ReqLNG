package de.schneidertim.reqlng.utility;

public interface IPOSRegexPattern {
	
	boolean matches(String text, String regex); 
	
	MatchResult match(String text, String regex); 
	
}
